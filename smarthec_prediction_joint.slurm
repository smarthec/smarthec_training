#!/bin/bash

#Submit this script with: sbatch thefilename

#SBATCH --partition=ml # Power9/ppc64le
#SBATCH --gres=gpu:1 # with 1 GPUs (must be specified here)
#SBATCH --time=15:00:00   # walltime
#SBATCH --nodes=1   # number of nodes
#SBATCH --ntasks-per-node=1  # limit to one node
#SBATCH --cpus-per-task=4  # number of processor cores (i.e. threads)
#SBATCH --mincpus=1 # CPUs per node
#SBATCH --mem-per-cpu=6000M   # memory per CPU core
#SBATCH -J smarthec_prediction   # job name
#SBATCH --array 0-10 # models/mdls (4 alt.mdl + 7 neu.mdl)
#SBATCH --mail-user=robert.sachunsky@slub-dresden.de   # email address
#SBATCH --mail-type=BEGIN,END,ARRAY_TASKS
#SBATCH -A p_da_layout
#SBATCH --chdir /projects/p_da_layout/ws

# todo: chain on "dtrsync -av smarthec/ ws/smarthec/" or "dttar -xvf waws/smarthec.tar -C ws"
# todo: multi-GPU
# todo: ressource optimization (GPU RAM, CPU RAM, CPUs, nodes, workers)

#export OMP_NUM_THREADS=$SLURM_CPUS_ON_NODE

set -e
# sync data from projects/
# dtrsync -av smarthec/ ws/smarthec/
# or from warm archive:
# dttar -xvf waws/smarthec.tar -C ws/

module load modenv/ml
module load TensorFlow/1.15.0-fosscuda-2019b-Python-3.7.4
#module load imkl/2019.5.281-iimpi-2019b
module load libjpeg-turbo/2.0.3-GCCcore-8.3.0
module use /home/rosa992c/my-modules
module load GEOS/3.9.0 # in ~/my-kernel/powerai-kernel/usr/local
source /home/rosa992c/my-kernel/powerai-kernel/bin/activate
#cd /scratch/ws/rosa992c-layout_rec/
cd /projects/p_da_layout/ws/smarthec
MDL=$SLURM_ARRAY_TASK_ID
if ((MDL<4)); then
    cd alt.mdl
else
    cd neu.mdl
    MDL=$(($MDL-4))
fi
MDLS=(Sonstige Brunata Ista Techem BFW Kalo Minol)
echo ${MDLS[$MDL]}
cd ${MDLS[$MDL]}
echo $PWD
# (neu.mdl includes symlinks to old datasets)
# (Sonstige includes symlinks to other MDLs)
# 
OUTFILE="smarthec_prediction.log"
>$OUTFILE

# find newest model
MODEL=${MDLS[$MDL]}
LOG=$(ls -1td logs/formdata* | head -1)
# test -nt/-ot is unusable because it always dereferences symlinks
if test ! -e $MODEL.h5 || test $(stat -c %Y $LOG) -gt $(stat -c %Y $MODEL.h5); then
    ln -fvrs -T $(ls -1t $LOG/mask_rcnn_formdata_*.h5 | head -1) $MODEL.h5
fi

# evaluate precision/recall and mAP:
for dataset in *.GT-COCO/*.coco.json; do 
    mkdir -p $(dirname ${dataset//GT/PRED})
    if test -e ${dataset//GT/PRED} -a $(stat -c %Y ${dataset//GT/PRED}) -gt $(stat -c %Y $MODEL.h5) -a $(stat -c %Y ${dataset//GT/PRED}) -gt $(stat -c %Y $dataset); then
	continue
    fi
    python /projects/p_da_layout/src/ocrd_segment/maskrcnn-cli/formdata.py --cwd --model $MODEL.h5 predict --dataset $dataset --dataset-pred ${dataset//GT/PRED} &>>$OUTFILE; 
done
echo finished
# next steps (in base OCR-D workspaces, off-cluster):
# - `maskrcnn-formdata merge` of all target-specific GT/pred COCOs 
#   into single GT/pred COCO, replacing alpha-masked image references by originals
# - `maskrcnn-formdata compare` of GT vs pred in all workspaces
#    (followed by `extract-evaluation-logs.sh` to aggregate into CSV)
# - `maskrcnn-formdata merge` of all workspace-specific GT/pred COCOs
#   into single GT/pred COCO, prefixing image file names, and keeping
#   only annotations for pred (to feed into COCO explorer)
