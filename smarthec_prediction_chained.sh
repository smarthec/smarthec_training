#!/bin/bash

set -eu

if test "x${1:-}" = x0; then
    DEP=
else
    test -n "${1:-}" && DEP="-d afterok:$1" || DEP=
fi

for batch in ${BATCH:-alt.mdl neu.mdl}; do
    for mdl in ${MDL:-Sonstige Brunata Ista Techem BFW Kalo Minol}; do
	test -d ws/smarthec/$batch/$mdl || continue
	export WORKDIR=$PWD/ws/smarthec/$batch/$mdl
	export DATASET=($(cd $WORKDIR; echo *.GT-COCO/*.coco.json))
	export DATASETS="${DATASET[*]}"
	# roughly 1.4it/sec, maximum 300 images per dataset ... 4min
	echo starting ${#DATASET[*]} array jobs for $batch.$mdl
	/home/rosa992c/my-kernel/powerai-kernel/bin/savelog -c 100 -nlp $WORKDIR/smarthec_prediction_chained.log
	sbatch -a 1-${#DATASET[*]}%10 -J smarthec_prediction_$batch.$mdl -t 8 $DEP smarthec_prediction_chained.slurm
    done
done
