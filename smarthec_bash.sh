# rsync -av smarthec/ ws/smarthec/
module load modenv/ml
module load TensorFlow/1.15.0-fosscuda-2019b-Python-3.7.4
#module load imkl/2019.5.281-iimpi-2019b
module load libjpeg-turbo/2.0.3-GCCcore-8.3.0
module use ~/my-modules
module load GEOS/3.9.0 # in ~/my-kernel/powerai-kernel/usr/local
source ~/my-kernel/powerai-kernel/bin/activate
#cd /scratch/ws/rosa992c-layout_rec/
cd /projects/p_da_layout/ws/smarthec
# for dir in */*/*.GT-COCO...
cd alt.mdl/Brunata
alias formdata="python /projects/p_da_layout/src/ocrd_segment/maskrcnn-cli/formdata.py"
formdata --help
