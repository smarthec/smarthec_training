#!/bin/bash

srun --partition=ml \
    --pty \
    --gres=gpu:1 \
    --time=00:10:00 \
    --nodes=1 --ntasks-per-node=1 --cpus-per-task=4 --mincpus=1 \
    --mem-per-cpu=6000M \
    -J smarthec_interactive \
     -A p_da_layout \
     --chdir /projects/p_da_layout/ws \
     bash -i
