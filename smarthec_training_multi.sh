#!/bin/bash

set -eu

PROJECT=$PWD

# pretrain alt.mdl
STATUS=($(sbatch smarthec_training_multi.slurm alt.mdl))
JOB0=${STATUS[3]}
DEP="-d afterok:$JOB0"

# retrain neu.mdl
STATUS=($(sbatch $DEP smarthec_training_multi.slurm neu.mdl))
JOB1=${STATUS[3]}
DEP="-d afterok:$JOB1"

# finetrain alt.mdl + neu.mdl
STATUS=($(sbatch $DEP smarthec_training_multi.slurm all))
JOB1=${STATUS[3]}
DEP="-d afterok:$JOB1"

sbatch $DEP smarthec_prediction_multi.slurm
