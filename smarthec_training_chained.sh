#!/bin/bash

# array tasks (from smarthec_training_chained.slurm):
# 0: alt.Sonstige (first phase)
# 1-3: alt.* (second phase)
# 4: neu.Sonstige (second phase)
# 5-10: neu.* (third phase)

set -eu

PROJECT=$PWD
export MODELOPTS TRAINOPTS

find ws/smarthec -name smarthec_training_chained.log -exec /home/rosa992c/my-kernel/powerai-kernel/bin/savelog -c 100 -nlp {} ";"

if test "x${1:-}" = x0; then
    DEP=
else
    test -n "${1:-}" && DEP="--dependency=afterok:$1" || DEP=
    NOTF="--mail-type=BEGIN,END --mail-user=robert.sachunsky@slub-dresden.de"
    STATUS=($(dtrsync $DEP $NOTF -av smarthec/ ws/smarthec/))
    JOB0=${STATUS[3]}
    DEP="-d afterok:$JOB0"
fi

# train staged on all layers and all samples, init from COCO:
MODELOPTS="--imgs-per-gpu 4 --logs logs --model ../../../mask_rcnn_coco.h5" # "--model imagenet"
TRAINOPTS="--exclude heads --increment 5 --symlink Sonstige.h5" # multi-staged (160 epochs)
#STATUS=($(sbatch smarthec_training_chained.slurm --array 0 --time 15:00:00 --dependency afterok:$JOB0))
for ((i=0; i<160; i+=5)); do
    STATUS=($(sbatch -J smarthec_training_phase1_$i -a 0 -t 2:00:00 $DEP smarthec_training_chained.slurm))
    JOB0=${STATUS[3]}
    DEP="-d afterok:$JOB0"
    MODELOPTS="--imgs-per-gpu 4 --logs logs --model last"
    TRAINOPTS="--increment 5 --symlink Sonstige.h5"
done

MODELOPTS="--imgs-per-gpu 4 --logs logs --model ../Sonstige/Sonstige.h5"
TRAINOPTS="--depth all --rate 1e-4 --epochs 80 --increment 5"
#STATUS=($(sbatch smarthec_training_chained.slurm --array 1-3 --time 7:30:00 --dependency afterok:$JOB0))
JOB1=$JOB0
DEP="-d afterok:$JOB0"
for ((i=0; i<80; i+=5)); do
    STATUS=($(sbatch -J smarthec_training_phase2alt_$i -a 1-3 -t 2:00:00 $DEP smarthec_training_chained.slurm))
    JOB1=${STATUS[3]}
    DEP="-d afterok:$JOB1"
    MODELOPTS="--imgs-per-gpu 4 --logs logs --model last"
done

MODELOPTS="--imgs-per-gpu 4 --logs logs --model ../../alt.mdl/Sonstige/Sonstige.h5"
TRAINOPTS="--depth all --rate 1e-4 --epochs 80 --increment 5 --symlink Sonstige.h5"
#STATUS=($(sbatch smarthec_training_chained.slurm --array 4 --time 7:30:00 --dependency afterok:$JOB1))
JOB1=$JOB0
DEP="-d afterok:$JOB0"
for ((i=0; i<80; i+=5)); do
    STATUS=($(sbatch -J smarthec_training_phase2neu_$i -a 4 -t 2:00:00 $DEP smarthec_training_chained.slurm))
    JOB1=${STATUS[3]}
    DEP="-d afterok:$JOB1"
    MODELOPTS="--imgs-per-gpu 4 --logs logs --model last"
done

MODELOPTS="--imgs-per-gpu 4 --logs logs --model ../Sonstige/Sonstige.h5"
TRAINOPTS="--depth all --rate 1e-4 --epochs 80 --increment 5"
#STATUS=($(sbatch smarthec_training_chained.slurm --array 5-10 --time 7:30:00 --dependency afterok:$JOB2))
JOB2=$JOB1
DEP="-d afterok:$JOB1"
for ((i=0; i<80; i+=5)); do
    STATUS=($(sbatch -J smarthec_training_phase3_$i -a 5-10 -t 2:00:00 $DEP smarthec_training_chained.slurm))
    JOB2=${STATUS[3]}
    DEP="-d afterok:$JOB2"
    MODELOPTS="--imgs-per-gpu 4 --logs logs --model last"
done

bash smarthec_prediction_chained.sh $JOB2
