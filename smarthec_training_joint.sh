#!/bin/bash

# array tasks (from smarthec_training_chained.slurm):
# 0-3: alt.* (first phase)
# 4-7: neu.* (second phase)
# 8-10: neu.* (first=only phase)

set -eu

PROJECT=$PWD

if test "x${1:-}" = x0; then
    DEP=
else
    test -n "${1:-}" && DEP="--dependency=afterok:$1" || DEP=
    NOTF="--mail-type=BEGIN,END --mail-user=robert.sachunsky@slub-dresden.de"
    STATUS=($(dtrsync $DEP $NOTF -av smarthec/ ws/smarthec/))
    JOB0=${STATUS[3]}
    DEP="-d afterok:$JOB0"
fi

# pretrain alt.mdl
STATUS=($(sbatch -a 0-3,8-10 $DEP smarthec_training_joint.slurm))
JOB0=${STATUS[3]}
DEP="-d afterok:$JOB0"
# retrain neu.mdl
STATUS=($(sbatch -a 4-7 $DEP smarthec_training_joint.slurm))
JOB1=${STATUS[3]}
DEP="-d afterok:$JOB1"

sbatch $DEP smarthec_prediction_joint.slurm
